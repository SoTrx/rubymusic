import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
  selector: '[ngxDebounceInput]',
})
export class DebounceInputDirective implements OnInit, OnDestroy {
  @Input() debounceTime = 500;
  @Output() debounceInput = new EventEmitter();
  private inputs = new Subject();
  private subscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.subscription = this.inputs.pipe(
      debounceTime(this.debounceTime)
    ).subscribe(e => this.debounceInput.emit(e));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('input', ['$event'])
  inputEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    this.inputs.next(event);
  }
}
