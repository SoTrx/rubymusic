import Timer = NodeJS.Timer;
import {Track} from "../utils/track.service";

export class AccurateTimer {
  private expected : number;
  private timeout : Timer;

  constructor(private track : Track, private interval : number){}

  public start(){
    this.expected = Date.now() + this.interval;
    this.timeout = setTimeout( () => this.step(), this.interval);
  }

  public stop(){
    clearTimeout(this.timeout);
  }
  private step(){
    const drift = Date.now() - this.expected;
    if (drift > this.interval) {
      // You could have some default stuff here too...
      //if (errorFunc) errorFunc();
    }
    this.track.currentTime++;
    //console.log(this.track.currentTime)
    this.expected += this.interval;
    this.timeout = setTimeout(() => this.step(), Math.max(0, this.interval-drift));
  }
}
