import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  private static readonly API_KEY: string = 'AIzaSyCkwKLMY0CVSExb3CTn6S4OJ1ONrSuleoM';
  private static readonly SNIPPET_ENDPOINT: string = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails';
  private static readonly LINK_REGEX = /^.*((m\.)?youtu\.be\/|vi?\/|u\/\w\/|embed\/|\?vi?=|\&vi?=)([^#\&\?]*).*/;

  constructor(public http: HttpClient) {
  }

  getVideoInfos(url : string) {
    const endpoint = `${YoutubeService.SNIPPET_ENDPOINT}&id=${this.getIdFromUrl(url)}&key=${YoutubeService.API_KEY}`;
     return this.http.get(endpoint);
  }

  getIdFromUrl(url: string): string{
    return url.match(YoutubeService.LINK_REGEX)[3];
  }

  static isYoutubeLink(url : string){
    return this.LINK_REGEX.test(url);
  }

  static parseTime(durationstr : string){
    const numbers = '\\d+(?:[\\.,]\\d{0,3})?';
    const weekPattern = `(${numbers}W)`;
    const datePattern = `(${numbers}Y)?(${numbers}M)?(${numbers}D)?`;
    const timePattern = `T(${numbers}H)?(${numbers}M)?(${numbers}S)?`;

    const iso8601 = `P(?:${weekPattern}|${datePattern}(?:${timePattern})?)`;
    const objMap = ['weeks', 'years', 'months', 'days', 'hours', 'minutes', 'seconds'];
    const pattern = new RegExp(iso8601);
    const duration : any =  durationstr.match(pattern).slice(1).reduce((prev, next, idx) => {
      prev[objMap[idx]] = parseFloat(next) || 0;
      return prev;
    }, {});

    const then = new Date(Date.now());

    then.setFullYear(then.getFullYear() + duration.years);
    then.setMonth(then.getMonth() + duration.months);
    then.setDate(then.getDate() + duration.days);
    then.setHours(then.getHours() + duration.hours);
    then.setMinutes(then.getMinutes() + duration.minutes);
    // Then.setSeconds(then.getSeconds() + duration.seconds);
    then.setMilliseconds(then.getMilliseconds() + (duration.seconds * 1000));
    // Special case weeks
    then.setDate(then.getDate() + (duration.weeks * 7));

    const now = new Date(Date.now());
    const seconds = (then.getTime() - now.getTime()) / 1000;
    return seconds;
  }

}
