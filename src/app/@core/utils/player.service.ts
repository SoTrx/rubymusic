import {Injectable} from '@angular/core';
import * as socketIo from 'socket.io-client';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class PlayerService {
  private static readonly SERVER_PORT: number = 8089;
  current: number;
  public connected: Promise<boolean>;
  private io;
  private resolve;
  private reject;
  private oldTracksSource = new BehaviorSubject<string[]>(null);
  public oldTracks = this.oldTracksSource.asObservable();

  constructor() {
    this.io = socketIo(`ws://vps558962.ovh.net:${PlayerService.SERVER_PORT}`);
    const that = this;
    this.connected = new Promise(function () {
      that.resolve = arguments[0];
      that.reject = arguments[1];
    });

    this.io.once('syncOld', (urls: string[]) => {
      this.oldTracksSource.next(urls);
    });
    this.io.once('connected', () => that.resolve(true));
    this.io.emit('connection');

    this.io.on('playError', () => console.log('playError'));
    this.io.on('addError', () => console.log('playError'));
  }

  async add(url: string): Promise<string> {
    await this.connected;
    const answProm = new Promise((res, rej) => {
      this.io.once(`addSuccess${url}`, (songId: string) => res(songId));
      this.io.once(`addError${url}`, () => rej('0'));
    });

    this.io.emit('add', url);

    return await answProm.catch(() => null);

  }

  play(id: string) {
    this.io.emit('play', id);
  }

  pause(id: string) {
    this.io.emit('pause', id);
  }

  stop(id: string) {
    this.io.emit('stop', id);
  }

  remove(id: string) {
    this.io.emit('remove', id);
  }

  volume(id: string, volume: number) {
    this.io.emit('volume', {id: id, volume: volume});
  }

  loop(id: string) {
    this.io.emit('loop', id);
  }

  unloop(id: string) {
    this.io.emit('unloop', id);
  }

  async fetch(id: string, time: number) {

    const answProm = new Promise((res, rej) => {
      this.io.once(`fetchSuccess${id}`, () => res(true));
      this.io.once(`fetchError${id}`, () => rej(false));
    });
    this.io.emit('fetch', {id: id, time: time});
    return await answProm.catch(() => null);
  }

  async getTime(id: string): Promise<number> {
    const answProm = new Promise((res, rej) => {
      this.io.once(`syncTimeSuccess${id}`, (songId: string) => res(songId));
      this.io.once(`syncTimeSuccess${id}`, () => rej('0'));
    });

    this.io.emit('syncTime', id);
    return await answProm.catch(() => null);
  }
}
