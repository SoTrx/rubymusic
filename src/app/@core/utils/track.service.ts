import {Injectable} from '@angular/core';
import {AccurateTimer} from '../data/accurate-timer';
import {YoutubeService} from './youtube.service';
import {PlayerService} from './player.service';
import {BehaviorSubject} from 'rxjs';


export class Track {
  playerId: string;
  name: string;
  artist: string;
  url: string;
  cover: string;
  duration: number;
  currentTime: number;
  timer: AccurateTimer;
  paused: boolean;
  streamed: boolean;
  volume: number;
  looping: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class TrackService {
  private tracksMap: Map<string, Track> = new Map();
  private changeSource = new BehaviorSubject<null>(null);
  public trackChange = this.changeSource.asObservable();

  constructor(private youtubeService: YoutubeService, private playerService: PlayerService) {
    this.playerService.oldTracks.subscribe(urls => {
      if (urls != null) {
        urls.forEach(url => this.addTrack(url));
      }

    });
  }

  get tracks() {
    return Array.from(this.tracksMap.values());
  }

  public static compareTrack(t1: Track, t2: Track) {
    return t1.name.toLocaleLowerCase().localeCompare(t2.name.toLocaleLowerCase());

  }

  public static regenerateTimer(t: Track) {
    t.timer = new AccurateTimer(t, 1000);
  }

  private static getDefaultTemplate(url: string) {
    return {
      playerId: null,
      name: null,
      url: url,
      artist: null,
      cover: null,
      duration: null,
      currentTime: 0,
      timer: null,
      paused: true,
      volume: 100,
      looping: false,
      streamed: false,
    };
  }

  public getTrackById(id: string): Track {
    return this.tracksMap.get(id);
  }

  public getTracksByURL(url: string): Track[] {
    return this.tracks.filter(t => t.url === url);
  }

  public async addTrack(url: string): Promise<Track> {
    await this.playerService.connected;
    const promTracks = [this.youtubeService.getVideoInfos(url).toPromise(), this.playerService.add(url)];
    const res = await Promise.all(promTracks).catch(e => null);
    if (!res) {
      return null;
    }
    const id = res[1];
    const item = res[0].items[0];
    const track: Track = Object.assign(
      TrackService.getDefaultTemplate(url),
      {playerId: id},
      {
        name: item.snippet.title,
        artist: item.snippet.channelTitle,
        cover: `//img.youtube.com/vi/${item.id}/0.jpg`,
        duration: YoutubeService.parseTime(item.contentDetails.duration),
      });
    TrackService.regenerateTimer(track);
    this.tracksMap.set(id, track);
    return this.getTrackById(id);
  }

  public removeTrack(id: string) {
    this.tracksMap.delete(id);
    this.triggerChange();
  }

  private triggerChange() {
    this.changeSource.next(null);
  }

}
