import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ElipsisPipe} from "./elipsis.pipe";
import { FilterPipe } from './filter.pipe';

@NgModule({
  declarations: [ElipsisPipe, FilterPipe],
  imports: [
    CommonModule
  ],
  exports : [
    ElipsisPipe,
    FilterPipe
  ]
})
export class AppWidePipeModule { }
