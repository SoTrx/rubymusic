import {Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { PlayerService } from '../../../@core/utils/player.service';
import {TrackService, Track} from '../../../@core/utils/track.service';


@Component({
  selector: 'ngx-player',
  styleUrls: ['./player.component.scss'],
  templateUrl: './player.component.html',
})
export class PlayerComponent implements OnDestroy, OnInit {
  @Input()
  @HostBinding('class.collapsed')
  collapsed: boolean;

  @Input()
  trackId: string;

  track: Track;
  player: HTMLAudioElement;
  shuffle: boolean;

  @Output() public onRemove = new EventEmitter<string>();
  @Output() public trackEmitter = new EventEmitter<Track>();

  constructor(private playerService: PlayerService, private trackService: TrackService) {

  }

  ngOnInit() {
    this.track = this.trackService.getTrackById(this.trackId);
    this.createPlayer();
  }


  ngOnDestroy() {
    /*this.player.pause();
    this.player.src = '';
    this.player.load();*/
  }

  playPause() {
    //Playing or paused, the track is still streaming to the serv
    this.track.streamed = true;
    if (this.track.paused) {
      this.playerService.play(this.track.playerId);
      this.track.timer.start();
    } else {
      this.playerService.pause(this.track.playerId);
      this.track.timer.stop();
    }
    this.track.paused = !this.track.paused;
    this.trackEmitter.emit(this.track);
  }

  stop() {
    this.playerService.stop(this.track.playerId);
    this.track.streamed = false;
    this.track.paused = true;
    this.track.currentTime = 0;
    this.track.timer.stop();

  }

  remove() {
    this.trackService.removeTrack(this.track.playerId);
    this.playerService.remove(this.track.playerId);
  }

  toggleShuffle() {
    //this.shuffle = !this.shuffle;
  }

  toggleLoop() {

    this.track.looping = !this.track.looping;
    if (this.track.looping) {
      this.playerService.loop(this.track.playerId);
    } else {
      this.playerService.unloop(this.track.playerId);
    }
  }

  setGraphicalVolume(volume: number) {
    this.track.volume = volume;
  }

  setRemoteVolume(volume: number) {
    this.playerService.volume(this.track.playerId, volume);
  }

  getVolume(): number {
    return this.track.volume;
  }

  setRemoteProgress(duration: number) {
    this.track.timer.stop();
    this.playerService.fetch(this.track.playerId, this.track.currentTime).then(() => {
      this.track.timer.start();
    });

  }

  setGraphicalProgress(duration: number){
      this.track.currentTime = this.track.duration * duration / 100;
    }

    getProgress(): number {
        return this.track.currentTime / this.track.duration * 100 || 0;
    }

    private createPlayer() {
        this.player = new Audio();
        //this.player.onended = () => this.next();
        //this.player.duration = 50;
        //this.setTrack();
    }
}
