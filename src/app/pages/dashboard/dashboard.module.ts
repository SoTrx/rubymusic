import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { PlayerComponent } from './player/player.component';
import { OrderModule } from 'ngx-order-pipe';
import {AppWidePipeModule} from '../../@core/app-wide-pipe/app-wide-pipe.module';
import {CoreModule} from '../../@core/core.module';
import {NbInputModule} from '@nebular/theme';
import {FormsModule} from '@angular/forms';


@NgModule({
  imports: [
    ThemeModule,
    OrderModule,
    AppWidePipeModule,
    NbInputModule,
    FormsModule,
  ],
  declarations: [
    DashboardComponent,
    PlayerComponent,
  ],
})
export class DashboardModule { }
