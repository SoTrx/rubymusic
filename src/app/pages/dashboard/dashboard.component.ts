import {ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {NbSearchService} from '@nebular/theme';
import {YoutubeService} from "../../@core/utils/youtube.service";
import {PlayerComponent} from "./player/player.component";
import {Track, TrackService} from "../../@core/utils/track.service";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ height: 0, opacity: 0 }),
            animate('1s ease-out',
              style({ height: 300, opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ height: 300, opacity: 1 }),
            animate('1s ease-in',
              style({ height: 0, opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class DashboardComponent implements OnInit {
  @ViewChildren(PlayerComponent) players: QueryList<PlayerComponent>;
  //https://www.youtube.com/watch?v=9e7pwv8a-cI

  /*ngOnInit(): void {
    this.players.changes.subscribe( changes => {
      changes.
    })
  }*/
  public searchText: string;
  //private songs : Map<string, boolean>;
  private tracks: Track[];

  constructor(private searchService: NbSearchService, private trackService : TrackService, private cd: ChangeDetectorRef) {

    //this.trackService.addTrack("https://www.youtube.com/watch?v=9e7pwv8a-cI");
    //this.trackService.addTrack("https://www.youtube.com/watch?v=mIYzp5rcTvU");
    const that = this;
    this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        if(YoutubeService.isYoutubeLink(data.term)){
          this.trackService.addTrack(data.term).then( t => {that.cd.detectChanges()});
        }

      });
      this.searchText="meh";
    this.trackService.trackChange.subscribe( () => this.cd.detectChanges());

  }

  get playingSongs(){
    return this.trackService.tracks.filter(s => s.streamed).sort( TrackService.compareTrack);
  }

  get stoppedSongs(){
    return this.trackService.tracks.filter(s => !s.streamed).sort(TrackService.compareTrack);
  }

  ngOnInit(){
    this.searchText=null;
  }


  /*updateTracks(track : Track){
    let matches = this.tracks.filter(t => t.url === track.url);
    //Multiples song with same URL
    if(matches.length > 1){
      //Must have different id
      const idMatches = matches.filter(t => t.playerId === track.playerId);
      //but could be unset if the song was just added
      if(idMatches.length === 0){
        matches = matches.filter(t => t.playerId === null);
      }
    }
    const index = this.tracks.indexOf(matches[0]);
    if(this.tracks[index].paused != track.paused){
      this.tracks[index].paused = track.paused;

    }
  }*/

}
